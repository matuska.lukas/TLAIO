from typing import Dict, List, Tuple, Callable
import random

import numpy

from trafficAI.constants import (
    ACCESS_POINT_IDENTIFIER, JUNCTION_POINT_IDENTIFIER, LINK_POINT_IDENTIFIER,
    LINK_LENGTH, EVALUATE_STEP, SPAWN_PROPABILITY
)


class Link:
    """
    One directional link between two traffic points

    When two connections are pointing to LinkPoint, the self.destination is
    actually another ``Link`` instead of ``Point`` object. Thats why the Link
    objects requires ``_redirect`` method to imitate point behaviour.

    Params:
        source: traffic point that puts vehicles on the link
        destination: traffic point that receives the vehicles from this link

    Args:
        queue: line on which the vehicles are placed and moved forward

    """
    def __init__(self, code: str, **kwargs) -> None:
        self.code = code
        self.destination = None
        self.identifier = None
        self.queue = []
        self.stucked = 0

    def register(self, destination: 'Point', identifier: str) -> None:
        """
        Finish the initialization of the link
        """
        self.destination = destination
        self.identifier = identifier

    def step(self) -> None:
        """
        Moves cars forward, or forwards them to the destination point
        """
        for i in range(len(self.queue)):
            position = self.queue[i]

            if position == LINK_LENGTH:
                if self.destination._redirect(self.identifier):
                    del self.queue[i]
            elif len(self.queue) - 1 == i or position + 1 < self.queue[i + 1]:
                # If vehicle is the last one or can move forward
                self.queue[i] += 1

    def reset(self) -> None:
        """
        Set the link to the initial state
        """
        self.queue = []
        self.stucked = 0

    def generate(self, space: int = 2) -> bool:
        """
        Spawn new vehicle on the road

        Args:
            space: the vehicle will be generated if the first `space` cells
                of the queue are empty

        Returns:
            (bool): True if generated successfully, False otherwise

        """
        empty = not self.queue or self.queue[0] > space

        if empty:
            self.queue.insert(0, 1)
        return empty


class Point:
    """
    Generic class that takes care about the grooping of the links
    """
    def __init__(self) -> None:
        self.connections = []

    def register_links(self, incoming: 'Link', outcoming: 'Link') -> None:
        """
        Register the links and set the identifier of the incomming link

        Args:
            incomming: Link which destination is set to this point, this Link
                gets an identifier which will use when requesting something
                from the Point, so the Point knows which Link made the request
            outcomming: Link in which the cars will be generated

        """
        identifier = len(self.connections)
        incoming.register(self, identifier)

        self.connections.append({'in': incoming, 'out': outcoming})


class LinkPoint(Point):
    """
    Point that is actually doing pretty much nothing besides connecting the
    links to each other.
    """
    def __init__(self) -> None:
        super().__init__()

    def _redirect(self, identifier):
        following = int(not identifier)
        return self.connections[following]['out'].generate()


class AccessPoint(Point):
    """
    Class that symbolize the border of the traffic area
    """
    def __init__(self) -> None:
        super().__init__()

        self.to_generate = 0
        self.callback = None

    def spawn(self) -> None:
        """
        Increase the counter of vehicles, that should be spawned
        """
        self.to_generate += 1

    def step(self) -> None:
        """
        Try to generate a vehicle
        """
        if not self.to_generate or random.random() > SPAWN_PROPABILITY:
            return

        link = random.choice(self.connections)['out']
        if link.generate(numpy.random.randint(2, 4)):
            self.to_generate -= 1

    def _redirect(self, *_) -> bool:
        """
        There is no redirection here. The vehicle just dissapears since it gets
        out of the traffic map. It is assumed that traffic beyond the system is
        perfect, therefore the point cannot be blocked. The controller have to
        be notified thought so new vehicle will be spawned to balance the total
        number of vehicles.
        """
        self.callback()
        return True

    def reset(self) -> None:
        """
        Set the point to the initial state
        """
        self.to_generate = 0


class JunctionPoint(Point):
    """
    Junction which may connect more than two connections. At the time only one
    incoming link isn't blocked.

    Args:
        open: maps outcoming links to boolean which determine if the link is
            closed. If true, vehicles pass, otherwise they are blocked.

    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def register_links(self, *args, **kwargs):
        """
        Set the queue links as closed from the beginning
        """
        super().register_links(*args, **kwargs)

        identifier = len(self.connections) - 1
        self.connections[identifier]['open'] = False

    def _redirect(self, identifier) -> bool:
        """
        Try to process a vehicles from opened link. The vehicle cannot be send
        to back to the point where it comes from.
        """
        if not self.connections[identifier]['open']:
            return False

        possible_identifiers = set(self.connections) - set([identifier])

        for possible_identifier in numpy.random.shuffle(possible_identifiers):
            if self.connections[possible_identifier]['out'].generate():
                return True
        return False

    def set(self, numbers):
        """
        Open the link based on the received number
        """
        # NOTE: yet to be done


class CoreController:
    """
    Initialize, connects and finally control traffic objects

    Params:
        minimap: positions of the points object in the matrice
        connections: tuples which specifies which points should be connected
            by links
        vehicles: number of vehicles in the system

    Attributes:
        TYPES: mapping of the Points identifier to its related class
        borders: contains all AccessPoint objects; used when spawning the
            vehicles into the system
        junctions: contains all JunctionPoint objects; used when executing
            the AI
        queues: contains all links which leads into the junctions; these links
            are the input for the NN
        points: contains all Point objects
        links: contains all Link objects

        cycle: cycle counter, incremented each step
        vehicle: desirable number of vehicles in the system
        nn: neural network used for the junction control

    """
    TYPES: Dict[str, 'Point'] = {
        ACCESS_POINT_IDENTIFIER: AccessPoint,
        JUNCTION_POINT_IDENTIFIER: JunctionPoint,
        LINK_POINT_IDENTIFIER: LinkPoint,
    }

    def __init__(self,
                 minimap: List[List[str]],
                 connections: List[Tuple[str, str]],
                 vehicles: int
                 ) -> None:
        self.borders: List['AccessPoint'] = []
        self.junctions: List['JunctionPoint'] = []
        self.queues: List['Link'] = []

        self.points: Dict[str, 'Point'] = {}
        self.links: List['Link'] = []

        self.cycle: int = 0
        self.vehicles: int = vehicles
        self.nn: Callable = None

        self._init_objects(minimap, connections)

    def _init_objects(self,
                      minimap: List[List[str]],
                      connections: List[Tuple[str, str]],
                      ) -> None:
        """
        Based on the `minimap` this method initialize required objects and
        connect them to each other according to the `connections`

        Args:
            minimap: 2D map of traffic points
            connections: connections between traffic points

        """
        for y, row in enumerate(minimap):
            for x, identificator in enumerate(row):
                if not identificator:
                    continue

                code = '{}.{}'.format(x, y)
                point = self.TYPES[identificator]()
                self.points[code] = point

                if identificator == ACCESS_POINT_IDENTIFIER:
                    point.callback = self._spawn_vehicle
                    self.borders.append(point)
                elif identificator == JUNCTION_POINT_IDENTIFIER:
                    self.junctions.append(point)

        [self._spawn_vehicle() for _ in range(self.vehicles)]
        [self._create_links(*conn) for conn in connections]

    def _spawn_vehicle(self) -> None:
        """
        Spawn one vehicle to the map
        """
        numpy.random.choice(self.borders).spawn()

    def _create_links(self, code1: str, code2: str) -> None:
        """
        Create links between two given points

        Two contradictory links are created. These links are registered to each
        traffic point and added to list of links.

        Links that heads into JunctionPoints are added to ``self.queues``

        Args:
            code1: The code of first traffic point
            code2: The code of second traffic point

        """
        point1 = self.points[code1]
        point2 = self.points[code2]

        link1 = Link('{}-{}'.format(code2, code1))
        link2 = Link('{}-{}'.format(code1, code2))

        point1.register_links(incoming=link1, outcoming=link2)
        point2.register_links(incoming=link2, outcoming=link1)

        if isinstance(point1, JunctionPoint):
            self.junction_queues.append(link1)
        if isinstance(point2, JunctionPoint):
            self.junction_queues.append(link2)

        self.links.extend((link1, link2))

    def reset(self,
              nn: Callable[[List[List[int]]], List[int]] = None,
              ) -> None:
        """
        Set the system to the initial state

        Since the reset is used mainly for the training of different
        NN, the method can also be used to inject the new NN

        Args:
            nn: neural network to be used

        """
        self.nn = nn

        [ap.reset() for ap in self.borders]
        [link.reset() for link in self.links]
        [self.spawn_vehicle() for _ in range(self.vehicles)]

    def step(self) -> None:
        """
        Perform one cycle of the celular automata

        Note:
            The controller asks the nn for the state of the traffic lights.
            This is beeing done repeatedly every `EVALUATE_STEP` cycles.

        """
        [link.step() for link in self.links]
        [ap.step() for ap in self.borders]

        self.cycle += 1
        if self.nn and self.cycle % EVALUATE_STEP == 0:
            self._execute_nn()

    def _execute_nn(self) -> None:
        """
        Gather data from the system and let the NN choose
        """
        # NOTE: yet to be finished
