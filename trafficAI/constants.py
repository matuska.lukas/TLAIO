"""
Includes constants

Args:
    ACCESS_POINT_IDENTIFIER: identifier to create and position the AccessPoint
        in the minimap
    JUNCTION_POINT_IDENTIFIER: identifier to create and position the
        JunctionPoint in the minimap
    LINK_POINT_IDENTIFIER: identifier to create and position the LinkPoint
        in the minimap

    LINK_LENGTH: how long are the links between the points
    EVALUATE_STEP: every `EVALUATE_STEP` simulation cycles will the NN be asked
        for the state of the traffic lights
    SPAWN_PROPABILITY: the propability of spawning the vehicle by a AccessPoint
        per every cycle

    CAPTION: the caption of the window
    SCREEN_SIZE: default screen size of the window when created

    LINK: slice of the road, the slices are put next to each other by the
        longer side which makes a tube. A line made of twos creates outcomming
        path, the line made of threes is incomming line.
    LINK_LENGTH: how much vehicles can fit into a link, also deternime how many
        ``LINK`` slices is placed to create the link

    UP, DOWN, LEFT, RIGHT: directions expressed in x, y

    CELL_LENGTH: the length of the cell in px
    CELL_SIZES: x, y sizes
    BACKGROUND_COLOR: RGB of the backgroud
    CELL_EMPTY: value of empty cell in cellular automata
    CELL_VEHICLE: value of cell where some vehicle is located
    CELL_CONNECTION_FROM, CELL_CONNECTION_TO: only used in ``LINK``, determine
        where the queue of the links should be displayes
    CELL_STOP, CELL_GO: used to describe the state of traffic lights
    CELL_COLORS: mapping of the cells codes to their collors

    POINT_HEIGHT: height of the traffic point, have to be odd number
    POINT_WIDTH: width of the traffic point, have to be odd number
    POINTS: mapping of the identifier to the points raster

"""
import os


# =============================================================================
#   Automata constants
# =============================================================================

ACCESS_POINT_IDENTIFIER   = 'a'
JUNCTION_POINT_IDENTIFIER = 'j'
LINK_POINT_IDENTIFIER     = 'l'

LINK_LENGTH = 20
EVALUATE_STEP = 30
SPAWN_PROPABILITY = 0.3


# =============================================================================
#   Graphics constants
# =============================================================================

# General
CAPTION = "TrafficAI"
SCREEN_SIZE = (1600, 1000)

UP    = ( 0, -1)
DOWN  = ( 0,  1)
LEFT  = (-1,  0)
RIGHT = ( 1,  0)

# Links
LINK = [1, 0, 3, 0, 2, 0, 1]
LINK_WIDTH = len(LINK)


# Cells
CELL_LENGTH = int(os.environ.get('CELL_LENGTH', 3))
CELL_SIZES = (CELL_LENGTH, CELL_LENGTH)

_NULL              = (200, 200, 200)
_ACTIVE_CELL_COLOR = (  0,   0,   0)
_RED_COLOR         = (255,   0,   0)
_GREEN_COLOR       = (  0,   0, 255)

BACKGROUND_COLOR = _NULL

CELL_EMPTY           = 0
CELL_VEHICLE         = 1
CELL_CONNECTION_FROM = 2
CELL_CONNECTION_TO   = 3
CELL_STOP            = 8
CELL_GO              = 9

CELL_COLORS = {
    CELL_EMPTY:           _NULL,
    CELL_VEHICLE:         _ACTIVE_CELL_COLOR,
    CELL_CONNECTION_FROM: _NULL,
    CELL_CONNECTION_TO:   _NULL,
    CELL_STOP:            _RED_COLOR,
    CELL_GO:              _GREEN_COLOR,
}

# Points
POINT_HEIGHT = 12
POINT_WIDTH  = 12

_LINK_POINT = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]
_ACCESS_POINT = [
    [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
]
_JUNCTION_POINT = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]
POINTS = {
    ACCESS_POINT_IDENTIFIER:   _ACCESS_POINT,
    LINK_POINT_IDENTIFIER:     _LINK_POINT,
    JUNCTION_POINT_IDENTIFIER: _JUNCTION_POINT,
}
