"""
This module presents the state of cellular automata using PyGame.
"""
import contextlib
from typing import Iterable, Tuple

with contextlib.redirect_stdout(None):
    import pygame

from trafficAI.constants import (
    CAPTION,
    LINK, LINK_LENGTH, LINK_WIDTH,
    POINTS, POINT_HEIGHT, POINT_WIDTH,
    CELL_LENGTH, CELL_SIZES,
    UP, DOWN, LEFT, RIGHT,
    CELL_COLORS, BACKGROUND_COLOR, CELL_EMPTY, CELL_VEHICLE,
    CELL_CONNECTION_FROM, CELL_CONNECTION_TO,
    SCREEN_SIZE,
)


class Presenter:
    """
    The class separates the logic of the drawing - the walls of connections and
    traffic points, the vehicles on the links - from everything else to improve
    the readability.

    This class does not flip the pygame display.

    Args:
        points_location: maps codes of traffic points to the position in the
            minimap
        link_vector: maps the name of the link to the position where the
            vehicles spawn onto the link and vector of vehicles direction

    """
    def __init__(self, minimap, connections, **kwargs):
        self.link_vector = {}

        self._init_window()
        self._draw_static_objects(minimap, connections)
        pygame.display.flip()

    def _init_window(self) -> None:
        """
        Initialize PyGame window
        """
        pygame.init()
        pygame.display.set_caption(CAPTION)

        screen = pygame.display.set_mode(SCREEN_SIZE, pygame.DOUBLEBUF)
        screen.fill(BACKGROUND_COLOR)

    def _draw_static_objects(self, minimap, connections):
        """
        Draw the walls of traffic points and save their location in minimap for
        later use, draw the walls of the connections and store their code,
        position and direction.

        Params:
            minimap: the 2D array of points
            connections: connections between the points in the minimap

        """
        # draw the points
        for row in range(len(minimap)):
            for column in range(len(minimap[row])):
                if not minimap[row][column]:
                    continue

                left_x, top_y = self._minimap_to_grid(row, column)
                raster = POINTS[minimap[row][column]]
                self._draw_object(left_x, top_y, raster)

        # draw the roads
        for source, destination in connections:
            self._proccess_connection(source, destination)

    @staticmethod
    def _minimap_to_grid(row: int, column: int) -> Tuple[int, int]:
        """
        Converts the position of the traffic point from the minimap to the
        position of the left top cell in cells grid unit
        """
        left_x = (LINK_LENGTH + POINT_WIDTH) * column
        top_y = (LINK_LENGTH + POINT_HEIGHT) * row

        return left_x, top_y

    @staticmethod
    def _draw_object(left_x: int,
                     top_y: int,
                     raster: Iterable[Iterable[int]],
                     ) -> None:
        """
        Draw an object described by raster

        Params:
            left_x: x position in cell's grid of the most left cells
            top_y: y position in cell's grid of the most top cells
            raster: 2D array of the object to be drawn

        """
        for shift_y, row in enumerate(raster):
            for shift_x, cell_value in enumerate(row):
                x = left_x + shift_x
                y = top_y + shift_y
                Presenter._draw_cell(x, y, CELL_COLORS[cell_value])

    @staticmethod
    def _draw_cell(x: int,
                   y: int,
                   color: Tuple[int],
                   ) -> None:
        """
        Convert cell's grid value to px and draw the cell

        The cell is only drawn but the surface is not flipped yet to show the
        changes

        Params:
            x: the x position of the cell in the grid
            y: the y position of the cell in the grid
            color: decimal RGB color {0...255}

        """
        x_px = x * CELL_LENGTH
        y_px = y * CELL_LENGTH
        rectangle = pygame.Rect((x_px, y_px), CELL_SIZES)
        pygame.draw.rect(pygame.display.get_surface(), color, rectangle)

    def _proccess_connection(self,
                             source: str,
                             destination: str,
                             ) -> None:
        """
        Draw a walls of the connection between two specified points and store
        the informations about links int the connection.

        To draw a connection, the ``SLICE`` is drawn in a direction determined
        by ``step_vector``. Then the position is moved by ``vector``, which
        determine the direction of the connection. This repeats. This process
        creates a tube which represents the connection.

        Params:
            source: code of the first point referred to as source from now on
            destination: second point with refference of destination

        """
        column, row, vector = self._get_source_info(source, destination)
        x, y, step = Presenter._calculate_start(column, row, vector)

        forward_code = '{}-{}'.format(source, destination)
        backwards_code = '{}-{}'.format(destination, source)

        for index in range(LINK_LENGTH):
            slice_x = x
            slice_y = y

            for cell in LINK:
                Presenter._draw_cell(slice_x, slice_y, CELL_COLORS[cell])
                if index == 0 and cell == CELL_CONNECTION_FROM:
                    self.link_vector[forward_code] = {
                        'xy': (slice_x, slice_y),
                        'vector': vector
                    }
                elif index == LINK_LENGTH - 1 and cell == CELL_CONNECTION_TO:
                    self.link_vector[backwards_code] = {
                        'xy': (slice_x, slice_y),
                        'vector': (-vector[0], -vector[1])
                    }

                slice_x += step[0]
                slice_y += step[1]

            x += vector[0]
            y += vector[1]

    def _get_source_info(self,
                         source_code: str,
                         dest_code: str,
                         ) -> Tuple[int, int, Tuple[int, int]]:
        """
        Calculate the direction from source point to the destination one and
        checks if the direction is supported one.

        Params:
            source_code: code of the source traffic point
            dest_code: code of the destination traffic point

        Returns:
            (int): column of the source traffic point
            (int): row of the source traffic point
            (Tuple[int, int]): vector of the direction to the destination

        """
        source = list(map(int, source_code.split('.')))
        dest = list(map(int, dest_code.split('.')))

        vector = (dest[0] - source[0], dest[1] - source[1])
        if vector not in (UP, DOWN, LEFT, RIGHT):
            raise RuntimeError("Unsuported road direction: {} for points: {} "
                               "and {}".format(vector, source_code, dest_code))

        return source[0], source[1], vector

    @staticmethod
    def _calculate_start(column: int,
                         row: int,
                         vector: Tuple[int, int]
                         ) -> Tuple[int, int, Tuple[int, int]]:
        """
        Calculate the position of the traffic point and based on the direction
        of the connection and sizes of connection and traffic point, calculates
        where the connection should be placed.

        Args:
            column: column of the source traffic point from which the
                connection should be drawn
            row: row of the source traffic point from which the connection
                should be drawn
            vector: vector that determine the direction of the connection

        Returns:
            (int): X position in grid cell unit; locates the starting point
            (int): Y position in grid cell unit; locates the starting point
            (Tuple[int, int]): determine the step vector

        """
        start_x, start_y = Presenter._minimap_to_grid(row, column)

        if vector == UP:
            shift_x = (POINT_WIDTH / 2) - (LINK_WIDTH / 2)
            shift_y = -1
            step_vector = RIGHT
        elif vector == DOWN:
            shift_x = (POINT_WIDTH / 2) + (LINK_WIDTH / 2) - 1
            shift_y = POINT_HEIGHT
            step_vector = LEFT
        elif vector == LEFT:
            shift_x = -1
            shift_y = (POINT_HEIGHT / 2) + (LINK_WIDTH / 2) - 1
            step_vector = UP
        elif vector == RIGHT:
            shift_x = POINT_WIDTH
            shift_y = (POINT_HEIGHT / 2) - (LINK_WIDTH / 2)
            step_vector = DOWN
        x = start_x + shift_x
        y = start_y + shift_y
        return x, y, step_vector

    def main_loop(self, core) -> None:
        miliseconds = 20
        before = pygame.time.get_ticks()

        while True:
            b = pygame.time.get_ticks()
            core.step()
            self._redraw_links(core.links)

            new = pygame.time.get_ticks()
            delta = new - before
            before = new
            wait_miliseconds = miliseconds - delta

            while True:
                if pygame.time.get_ticks() - b > miliseconds:
                    break

            if wait_miliseconds > 0:
                pygame.time.wait(wait_miliseconds)

            pygame.display.flip()

    def _redraw_links(self, links: Iterable['automata.Link']) -> None:
        """
        Redraw the vehicles on the links due to recent changes

        Params:
            links: list of links which should be redrawn

        """
        for link in links:
            info = self.link_vector[link.code]

            x, y = info['xy']
            vector = info['vector']
            for index in range(LINK_LENGTH):
                cell = CELL_VEHICLE if index in link.queue else CELL_EMPTY
                self._draw_cell(x, y, CELL_COLORS[cell])

                x += vector[0]
                y += vector[1]
